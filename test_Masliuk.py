import requests
import json
import time

#Задаем источник данных в виде API с https://www.coingecko.com/en/api/documentation
BIT_API='https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd'
ETH_API='https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=usd'

#Определяем функцию для получения данных о курсе BIT и ETH из источника данных
def get_price(API_TYPE):
    crypto_type=API_TYPE[API_TYPE.find('ids=')+4:API_TYPE.find('&vs')]
    response=requests.get(url=API_TYPE)
    data = json.loads(response.text)
    price= int(data[f"{crypto_type}"]["usd"])
    return price

#Отправляем запросы к API каждую 1 минуту для обновления значения курса BIT и ETH
#Сохраняем историю запросов в списки
price_hisory_BIT = [] #price_hisory_BIT - список, куда при обновлении цены записывается текущая цена BIT
price_hisory_ETH = [] #price_hisory_ETH - список, куда при обновлении цены записывается текущая цена ETH
REAL_ETH_change_history=[] #REAL_ETH_change_history - список, куда при обновлении цены записывается текущая цена ETH, без учета влияния BIT
koff_k=[] #koff_k - список, куда при обновлении цены записывается коэффициент, определяющий текущую линейную зависимость ETH от BIT

while True: #Цикл выполняющийся до ручно остановки программы

    NOW_price_BIT=get_price(BIT_API) #обращаемся к ранее определенной функции get_price(API_TYPE) с аргументом BIT_API для получения текущего курса BIT
    price_hisory_BIT.append(NOW_price_BIT) #добавляем текущий курс BIT в список price_hisory_BIT
    NOW_price_ETH = get_price(ETH_API) #обращаемся к ранее определенной функции get_price(API_TYPE) с аргументом ETH_API для получения текущего курса ETH
    price_hisory_ETH.append(NOW_price_ETH) #добавляем текущий курс ETH в список price_hisory_ETH
    print('Текущая стоимость BIT = ', NOW_price_BIT)
    print('Текущая стоимость ETH = ', NOW_price_ETH)
    time.sleep(60) #берем паузу в 60 секунд, длительность которой определяет то, как часто программа будет запрашивать текущий курс BIT и ETH. Данная цифра в данный момент ограничена бесплатным тарифом на API
    if len(price_hisory_ETH)>=5: #данно условие добавлено, чтобы накопить достаточное количество данных, для рассчета k_final (коэффициента зависимости ETH от BIT)
        # Далее с помощью цикла for усредняем значение коэффициентов зависимости k_1 ETH от BIT за последние 5-60 минут (в зависимости от наличия данных)
        #
        for i in range(0,len(price_hisory_ETH)):
            k_1 = price_hisory_ETH[i]/price_hisory_BIT[i]
            koff_k.append(k_1)
        #использовать значения в списке koff_k решено за последние 60 минут, т.к. рынок непредсказуцем, а коэффициент зависимости может имзмениться.
        k_final = sum(koff_k[-60:])/len(koff_k[-60:]) #получаем итоговый средний линейный коэффициент зависимости курса ETH от BIT
        print('Средняя цена ETH относительно средней цены BIT за последние 60 мин = ',k_final)
        #считаем собственное движение ETH(без влияния BIT)
        REAL_ETH_change = abs(NOW_price_BIT*k_final-NOW_price_ETH)
        REAL_ETH_change_history.append(REAL_ETH_change)
        Min_REAL_ETH_last_60min = min(REAL_ETH_change_history[-60:])
        Max_REAL_ETH_last_60min = max(REAL_ETH_change_history[-60:])
        if ((Max_REAL_ETH_last_60min-REAL_ETH_change)/Max_REAL_ETH_last_60min)>=0.01:
            print(f'ВНИМАНИЕ! Произошло изменении цены на 1% или более за последние 60 минут.\n Максимальная цена за 60 минут={Max_REAL_ETH_last_60min}.\n Текущая цена (без учета BIT)={REAL_ETH_change}.')
        elif ((REAL_ETH_change - Min_REAL_ETH_last_60min) / Min_REAL_ETH_last_60min) >= 0.01:
            print(f'ВНИМАНИЕ! Произошло изменении цены на 1% или более за последние 60 минут.\n Минимальная цена за 60 минут={Min_REAL_ETH_last_60min}.\n Текущая цена (без учета BIT)={REAL_ETH_change}.')




